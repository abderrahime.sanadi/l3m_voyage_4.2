<?php

namespace App\Form;

use App\Entity\Destination;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DestinationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, ["label" => "Nom de la Destination", "attr" => ['class' => "form-control mb-2", "placeholder" => "Nom de la Destination..."]])
            ->add('images', FileType::class, ["mapped" => false, "required" => false, "multiple" => true, "data_class" => null])
            ->add('price', TextType::class, ["label" => "Prix en DHs : ", "attr" => ['class' => "form-control mb-2 mt-2"]])
            ->add('travelTime', TextType::class, ["label" => "Durée du voyage en heure: ", "attr" => ['class' => "form-control mb-2 mt-2"]])
            ->add('description', TextareaType::class, ["label" => "Description :", "attr" => ['class' => "form-control mb-2 mt-2", "rows" => 6]]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Destination::class,
        ]);
    }
}
