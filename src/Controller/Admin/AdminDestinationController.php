<?php

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\DestinationType;
use App\Entity\Destination;
use App\Entity\Image;
use App\Repository\DestinationRepository;
use App\Services\ImageService;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\HttpFoundation\Request;

/**
 * Require ROLE_ADMIN for *every* controller method in this class.
 *
 * @IsGranted("ROLE_ADMIN")
 */

class AdminDestinationController extends AbstractController
{

    /**
     * @Route("/", name="app_home")
     */
    public function home(): Response
    {
        return $this->redirectToRoute('app_login');
    }

    /**
     * @Route("/admin/destinations", name="admin_destinations")
     */
    public function index(DestinationRepository $destinationRepository): Response
    {
        $destinations = $destinationRepository->findAll();
        return $this->render('admin_destination/list.html.twig', [
            'destinations' => $destinations,
        ]);
    }

    /**
     * @Route("/admin/destination/createOrEdit/{id}", name="destination_createOrEdit")
     */
    public function createOrEdit($id, Request $request, EntityManagerInterface $em, ImageService $imageService, ValidatorInterface $validator): Response
    {
        $images_path = [];
        if ($id == 0) {
            $destination = new Destination();
        } else {
            $destination = $em->getRepository(Destination::class)->find($id);
            $images = $em->getRepository(Image::class)->findBy(['destination' => $destination->getId()]);
            foreach ($images as $image) {
                array_push($images_path, $image->getPath());
            }
            if (!$destination) {
                throw $this->createNotFoundException(
                    'destination introuvable '
                );
            }
        }

        $form = $this->createForm(DestinationType::class, $destination);
        $form->handleRequest($request);

        $errors = $validator->validate($destination);
        if (count($errors) > 0) {
            $this->addFlash(
                'danger',
                (string) $errors
            );
            return new Response((string) $errors, 400);
        }
        if ($form->isSubmitted() && $form->isValid()) {

            $images = $form['images']->getData();
            if ($images) {
                foreach ($images as $img) {
                    $image = new Image();
                    $newFilename = $imageService->uploadImage($img);
                    $image->setPath('images/' . $newFilename);
                    $image->setDestination($destination);
                    $em->persist($image);
                }
            }
            /*$slugger = new AsciiSlugger();
            $slug = $slugger->slug($form['name']->getData());
            $destination->setSlug($slug);
            */

            $msg = 'Destination crée avec succès!';
            if ($destination->getId() != 0) {
                $msg = 'Destinationt modifié avec succès!';
            } else {

                $em->persist($destination);
            }

            $this->addFlash(
                'notice',
                $msg
            );
        }
        $em->flush();

        //return $this->redirectToRoute('Destination_index');

        return $this->render('admin_destination/createOrEdit.html.twig', [
            'form' => $form->createView(),
            'images_path' => $images_path
        ]);
    }

    /**
     * @Route("/admin/destination/remove/{id}", name="destination_remove")
     */
    public function remove(Destination $Destination,  EntityManagerInterface $em)
    {
        // TODO
        $em->remove($Destination);
        $em->flush();
        return $this->redirectToRoute('admin_destinations');
    }
}
